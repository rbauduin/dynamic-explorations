﻿open System.IO.IsolatedStorage
// Learn more about F# at http://fsharp.org


// for System.Convert type conversion
open System



// for Assembly
open System.Reflection

// for Regex.IsMatch
open System.Text.RegularExpressions
open Microsoft.FSharp.Core
open Error
open Xunit.Sdk

let globalIterator<'a> = fun (k:string) (v:'a) -> printfn "%s" k


// for users fixtures
open System.Collections.Generic

// for reading file, for yaml fixtures
open System.IO

open FSharp.Data.Dapper

// reflections for fixtures
open Microsoft.FSharp.Reflection


open System.IO
open System.Reflection
open Microsoft.FSharp.Reflection
open System
open Humanizer


open User
open FSharp.Interop.Dynamic

// function to read a yaml file and a Map<string,recordType>
// the name of the yamlfile to read, the type of records in the map are
// based on the string passed as argument.
let getRecordsMap (fixture:string) = 
    // file path
    let file_path = sprintf "fixtures/%s.yml" ((fixture.Pluralize().Underscore()))

    let yaml = File.ReadAllText(file_path)

    // records type
    let typeName = sprintf "%s+%s" fixture fixture
    let recordType = System.Type.GetType(typeName)

    // type of the map returned
    let genericMapType = typedefof<Map<_,_>>
    // note that you pass the array of types in the order of them appearing in the generic type definition
    // This is the same for functions, eg myFunc<T1,T2>
    let yamlType = genericMapType.MakeGenericType([| typeof<string>; recordType |])


    
    (* 
    // list assemblies loaded and look for the type in them
    AppDomain.CurrentDomain.GetAssemblies() |> Array.iter (fun i -> (printfn "iter: %s %A" (i.GetName().FullName) (i.GetType typeName)  ))

    let a = Assembly.Load("tests")
    let t = a.GetType("User")
    printfn "type from assembly = %A" t.FullName
    *)

    
    let fields = FSharpType.GetRecordFields (recordType)

    // printfn "--fields = %A " fields
    // printfn "--field names = %A " ( fields |> Seq.map (fun t -> t.Name) )
    // printfn "--field attrs = %A " ( fields |> Seq.map (fun t -> t.Attributes) )
    // printfn "--parent record types = %A " ( fields |> Seq.map (fun t -> t.ReflectedType ) )
    // printfn "--field types = %A " ( fields |> Seq.map (fun t -> t.PropertyType ) )


    // get the generic deserialize method and dynamically set our run time determined types
    let legivelAssembly = Assembly.GetAssembly(typeof<Legivel.Serialization.DeserializeResult<_>>)
    let deserializeMethod = legivelAssembly.GetType("Legivel.Serialization").GetMethod("Deserialize")
    let genericMethod = deserializeMethod.MakeGenericMethod(yamlType)

    // invoke the deserialize method, passing on argument, the yaml string
    let result = genericMethod.Invoke(null, [|yaml|])
    result

    //********************************************************************************

let getRecordOfType<'a>() =
    let file_path = sprintf "fixtures/%s.yml" ((typeof<'a>.Name.Pluralize().Underscore()))
    let yaml = File.ReadAllText(file_path)
    Legivel.Serialization.Deserialize<Map<string, 'a>>(yaml)

let iteratorOfType<'a> = fun (k:string) (v:'a) -> printfn "%s" k


let handleResult r =
    match r with
    | Legivel.Serialization.Succes info :: _  -> info.Data
    | Legivel.Serialization.Error e :: _      -> failwithf "YamlParse Error %A"  Error
    | []                                      ->  Map.empty

let instanciateGeneric funcName (types: Type []) =
    Assembly.GetExecutingAssembly().GetType("Program").GetMethod(funcName).MakeGenericMethod( types )

    // *******************************************************************

let processRecords<'a> () =
    let records = getRecordOfType<'a>() |> handleResult
    Map.iter<string,'a> iteratorOfType<'a> records

let processRecordsArgs (t:Type) =
    let processRecordInstance = instanciateGeneric "processRecords" [|t|]
    processRecordInstance.Invoke(null,[||])|> ignore

let processRecordsArray (names:string[]) =
    names |> Array.iter ( fun n ->
                             let typeName = sprintf "%s+%s" n n
                             let recordType = System.Type.GetType(typeName)
                             processRecordsArgs recordType
                        )

[<EntryPoint>]
let main argv =

    // handle option types in records as nullable
    OptionHandler.RegisterTypes()

    // read data from the fixtures/records.yml file
    let records = getRecordsMap("User")

    // We get a list, of which the first element in of type DeserializeResult
    //printfn "obtained records = %A " records
    //printfn "of type %A" (records.GetType())
    let records_head = (records?head)
    //printfn "head = %A " records_head
    //printfn "type of head = %A " (records_head.GetType())

    // We can identify the union case and its fields with https://msdn.microsoft.com/en-us/visualfsharpdocs/conceptual/fsharpvalue.getunionfields-method-%5Bfsharp%5D
    // We get a tupe UnionCaseInfo * obj[], but are only interested in the second element, a sequence of objects, of which we extract the first one
    let ttt = FSharpValue.GetUnionFields(records_head, records_head.GetType())
    let (_,ttt2)= ttt
    // printfn "ttt2 = %A " (ttt2)
    // printfn "ttt2 type = %A " (ttt2.GetType())
    let ttt3 = ttt2 |> Seq.head
    // We have a record { Data : }, so let's extract the value of the unique field Data:
    // printfn "DEBUG ttt3 : %A" ttt3

    let ttt4 = FSharpValue.GetRecordFields(ttt3) |> Seq.head

    // We now have a instance of Map<string,User>, but this is the runtime type, the static type is obj!
    // Thats why we can just use it as a map. As an illustration, the next line will cause the compiler to raise this error:
    //     This expression was expected to have type 'Map<string,'a>' but here has type 'obj'
    //Map.iter (fun k v -> printfn "%s" k) ttt4
    // That's why we will need to work with handling all this ourself below
    //
    // printfn "DEBUG ttt4 : %A" ttt4
    // printfn "DEBUG: %A " (ttt4.GetType())
    // printfn "DEBUG: %A " (ttt4.GetType().GetMethods())

    // first get the runtime type for the records in the map
    let typeName = sprintf "%s+%s" "User" "User"
    let recordType = System.Type.GetType(typeName)

    // build the runtime type of the map itself
    // we start by getting the generic type, and then "instanciate" it with MakeGenericType, passing the types left blank earlier in order.
    let genericMapType = typedefof<Map<_,_>>
    let mapType = genericMapType.MakeGenericType([| typeof<string>; recordType |])
    printfn "mapType = %A " mapType



    //
    // this doesn't work, we need to get the iter method from the module!
    // let iterMethod = mapType.GetMethod("iter")
    // printfn "iterMethod = %A " iterMethod
    //
    // Getting a handle on the module Map is not so easy. First ,its name is MapModule. then:
    // This doesn't work, not sure why (solution is below to use exported types)
    // let mapmod = Assembly.GetAssembly(genericMapType).GetModule("MapModule")
    // printfn "mapmod = %A" mapmod
    //
    // First working solution is to look in exported types for the MapModule
    let assembly = Assembly.GetAssembly(genericMapType)
    let modu = assembly.GetExportedTypes()|> Array.filter (fun m -> Regex.IsMatch(m.Name, "MapModule")) |> Array.head
    printfn "found module = %A" modu

    // Second working solution is to get the types from the mapType assembly:
    let map = mapType.Assembly.GetTypes() |> Array.find(fun t -> t.Name = "MapModule")
    printfn "found map = %A" map


    // get the MapModule.iter function
    // Note it has the name Iterate, having the annotation [<CompiledName("Iterate")>]``
    // https://github.com/fsharp/fsharp/blob/master/src/fsharp/FSharp.Core/map.fs#L685
    let iter = map.GetMethods(BindingFlags.Static ||| BindingFlags.Public) |> Array.find (fun m -> m.Name = "Iterate")

    // Iterators
    // create a unction that we want to pass to Map.iter
    // we cannot use the static operator in our case, as the recordType is a runtime determined type.
    let staticIterator (k:string) (v:User.User) =
        printfn "%s" k
    // This is the generic function
    let iterator = fun k v -> printfn "%s" k

    // We need to make build the dynamic function with https://msdn.microsoft.com/visualfsharpdocs/conceptual/fsharpvalue.makefunction-method-%5bfsharp%5d
    // But it has type signature Type * (obj -> obj) -> obj
    // iterator function has signature (k:string) -> (v:recordType) -> unit
    //                                            |                  ^ innerFunction
    //                                         outerFunction
    // meaning it can only define 1 argument functions. So we need to define 2 functions.
    // the inner function has type recordType -> unit
    // the outer function has type string -> innerFunctionType
    // We then can build the function with FSharpValue.MakeFunction, passing 2 arguments:
    // - the type of the signature build with MakeFunctionType
    // - the one argument lambda of signature obj -> obj.
    // The lambda passed is of type obj->obj, but thanks to the type signature passed in the
    // first argument, the lambda is "type casted" to the right type signature.

    let innerFunctionType = FSharpType.MakeFunctionType ( recordType, typeof<unit>)
    let outerFunctionType = FSharpType.MakeFunctionType ( typedefof<string>, innerFunctionType )

    // here we build the function by passing the lambda code in place.
    // Notice we need to do the typecase ourself. At definition time, the function receives an object v.
    // But at call time, due to the function signature (innerFunctionType), that object will be a string
    // and we can do the cast.
    // As return value, we cast unit to Object.
    // ----- locally defined lambda
    let iteratorFunc = FSharpValue.MakeFunction(outerFunctionType, fun k ->
                                                               FSharpValue.MakeFunction( innerFunctionType,  (fun v -> printfn "%s" (k:?>string) ; (() :> Object ) ) ))

    // ----- local function call
    // here we simply call the local function defined higher, also with needed casts.
    let genericIteratorFunc = FSharpValue.MakeFunction(outerFunctionType, fun k ->
                                                               FSharpValue.MakeFunction( innerFunctionType,  (fun v -> iterator (k:?>string) v :>Object) ) )

    // ----- global function
    // Here we get a handle to the globalIterator function defined in this file.
    // This had to be defined as a global function (not under main) to be able to get a handle on it
    let a = System.Reflection.Assembly.GetExecutingAssembly()
    let modu = a.GetType("Program")
    let methodInfo = modu.GetMethod("globalIterator")
    let closedIterator = methodInfo.MakeGenericMethod(recordType)
    // as we have a methodinfo in this case, we need to call Invoke on it.
    // The first argument it null because we don't call a method on an object.
    // In this case, we don't have to do casts.
    let closedIteratorFunc = FSharpValue.MakeFunction(outerFunctionType, fun k ->
                                                               FSharpValue.MakeFunction( innerFunctionType,  (fun v -> closedIterator.Invoke(null, [|k;v|]) ) ))


    // now we can get to work.
    // specialise the generic function
    let genericIter = iter.MakeGenericMethod([| typeof<string>  ; recordType |])

    // Check it works with the iterator with static types
    printfn "static iterator invocation:"
    genericIter.Invoke(null, [| staticIterator ; ttt4 |]) |> ignore
    // And here is what we wanted to do
    //invoke the Map.iter function with our iterator function, and the map instance ttt4
    // but this is a methodinfo on which to call invoke....
    printfn "iteratorFunc invocation:"
    genericIter.Invoke(null, [| iteratorFunc ; ttt4 |]) |> ignore
    printfn "genericIteratorFunc invocation:"
    genericIter.Invoke(null, [| genericIteratorFunc ; ttt4 |]) |> ignore
    printfn "closedIteratorFunc invocation:"
    genericIter.Invoke(null, [| closedIteratorFunc ; ttt4 |]) |> ignore


    printfn "********************************************************************************"
    // A statically typed approach
    let typedResult = getRecordOfType<User>()
    let typedMap = typedResult |> handleResult
    printfn "statically typed approach:"
    Map.iter<string,User> iteratorOfType typedMap

    printfn "################################################################################"
    printfn "best approach (in our example) call to one function processRecords<'t>():"
    // if your type is in a variable, the trick is to define a typed function at the highest possible
    // level of the call hierarchy
    // Eg, this will be cumbersome as the Map.iter won't be accespted by the compiler (typedUserRecords is obj, and not Map<'a,'b>):
    //    let instanciatedIterator = instanciateGeneric "iteratorOfType" [|recordType|]
    //    let getUserRecords = instanciateGeneric "getRecordOfType" [|recordType|]
    //    let typedUserRecords = getUserRecords.Invoke(null, [||])
    //    Map.iter (fun k v -> instanciatedIterator.Invoke(null,[|k;v|])|> ignore ) typedUserRecords
    // So the best approach is to defined a processRecords<'a> function, which we will instanciate and invoke in
    // the processRecordsArgs function
    processRecordsArgs typeof<User> |> ignore
    printfn "********************************************************************************"
    printfn "we can add a wrapping function accepting an array of strings, converting these to the right types"
    printfn "Here we load ` [|\"User\";\"User\"|]`"
    processRecordsArray [|"User"; "User"|]


    // ********************************************************************************
    // In the next part we look at printf like functions
    // Most reflection functions used are listed at
    // https://msdn.microsoft.com/en-us/visualfsharpdocs/conceptual/reflection.fsharptype-class-%5bfsharp%5d
    // We can write a function whose argument is of type PrintfFormat<'PrintfType,_,_,'ReturnType'>.
    // This means the argument of the function is a format string like "my string is %s".
    // The compiler will translform it in a PrintfFormat instance.
    // The PrintfFormat type is parameterised with 4 types, of which the first on is
    // denoted 'PrintfType in our example and the last is 'ReturnType.
    // ReturnType is the type of the value returned by our printf function. We will set it
    // as Unit for now.
    // PrintfType is the the function of the printf function handling that format string.
    // From it, we will be able to extract the type of the values needed to be passed for these
    // substitutions to be successful
    //
    // We can check this by printing the type:
    let f (format:PrintfFormat<'PrintfType,_,_,Unit>)=
        printfn "printftype = %A" typeof<'PrintfType>
    f "number %d string %s float %f"

    // The type printed is (after edition for simplification):
    // FSharpFunc`2[Int32,FSharpFunc`2[String,FSharpFunc`2[Double,Unit]]]
    // This is because FSharp functions only take one argument (cf currying).
    // So to print all types of the values needed to fill the format type,
    // we will need to write a recursive function.
    // Note that rather than printing, we could accumulate the types in a list for example.
    let f2 (format:PrintfFormat<'PrintfType,_,_,Unit>)=
        // this recursive function will print the arg types of the function type
        // it gets as argument.
        let rec printArgTypes typeArg=
            let domain,range = FSharpType.GetFunctionElements typeArg
            printfn "%A" domain
            if FSharpType.IsFunction range then
                printArgTypes range
        printArgTypes typeof<'PrintfType>
        0
    f2 "number %d string %s float %f"

    // To consume the format string argument, we can follow the same idea: keep
    // returning a function consuming one argument until we reach the end of the
    // types extracted from the format string
    let f3 (format:PrintfFormat<'PrintfType,_,_,Unit>):'PrintfType=
        // The trick here is to write a function that will take 2 arguments
        // - its own type signature in a `Type` instance
        // - one object. This will be one value passed to the format string
        // The trick is to set the first parameter value with partial application. That
        // way, the function's signature is available in its body. It will be used to
        // set the recursively defined and called function's signature.
        // This partial application leaves us with a one object argument function. 
        // We then set its type signature with FSharpValue.MakeFunction.
        // This last step has 2 effects:
        // - the parameter of this one-argument function is not of type object anymore,
        //   but of the type specified in the format string. Without this, there is a type
        //   mismatch between the function argument and the value passed to the format string.
        // - it sets its return type, so that we can have recursive calls as needed
        //   to handle multiple values passed to the format string. In this case, a one argument 
        //   function will be returned. To construct that one argument function, we again
        //   fix the two first arguments, and specialise the one argument function based
        //   on the return type of the current call.
        // For possibly a better understanding, read (1) then (2)
        let rec consumer (myType:Type) (v:obj) =
            //(2a)
            // v is of the type set by the function signature, as can be checked by printing it:
            printfn "%A %A" v (v.GetType())
            // (2b)
            // We must decide if we have more arguments to handle.
            // myType is the type signature that was assigned to us. So looking at it we know
            // if we need to return a function by looking at our range.
            let domain,range = FSharpType.GetFunctionElements myType
            // (2c)
            // if the range is a function, we do like explained in (1)
            if FSharpType.IsFunction range then
                let internalConsumer = consumer range
                box (FSharpValue.MakeFunction(range, internalConsumer))
            else
                // finished, we return unit currently (the 'ReturnType)
                // box it to return an object type
                box ()
        // fix the 2 first arguments, gicing u a one-object-argument function.        
        let argsConsumer = consumer typeof<'PrintfType>
        // (1)
        // We then set the one argument function signature as needed, based on the type
        // we get from PrintfFormat. This function will thus consume one value passed to 
        // the format string. When it executes, it will look at its signature (passed in the 
        // first argument) if it needs to return another function (to consume an additional value
        // passed to the format string) or not (in which case we make it return unit, as that
        // is what we set with `PrintfFormat<'PrintfType,_,_,Unit>`)
        // We need to unbox otherwise it is an object. Unboxing makes it a function of the
        // inferred type
        unbox (FSharpValue.MakeFunction(typeof<'PrintfType>, argsConsumer))
        
    printfn "calling f3"
    let res = f3 "number %d string %s float %f"  3 "test" 4.3

    // From here, it is trivial to accumulate the values and the types, and pass that information
    // to a function. Here's an example
    let f4 (handler: Type list-> obj list -> 'ReturnType) (format:PrintfFormat<'PrintfType,_,_,'ReturnType>):'PrintfType=
        let rec consumer (myType:Type) (typesList:Type list) (valuesList:obj list) (v:obj) =
            let domain,range = FSharpType.GetFunctionElements myType
            // accumulate types and lists
            let nextTypesList = (domain::typesList) 
            let nextValuesList = (v::valuesList)
            if FSharpType.IsFunction range then
                let internalConsumer = consumer range nextTypesList nextValuesList
                // MakeFunction returns an object, no need to box here
                FSharpValue.MakeFunction(range, internalConsumer)
            else
            // pass full lists to our handler
            // note that values have the right type in the handler
                // box the return value to have an object
                box (handler (List.rev nextTypesList) (List.rev nextValuesList))
        let argsConsumer = consumer typeof<'PrintfType> [] []
        // MakeFunction returns an obj. We unbox it to have instance of PrintfType,
        // which is the return type we explicitly set above
        unbox (FSharpValue.MakeFunction(typeof<'PrintfType>, argsConsumer))
        
    printfn "calling f4"
    let myHandler types values =
        printfn "types = %A" types
        printfn "values = %A" values
        values |> List.iter (fun v -> printfn "value %A has type %A" v (v.GetType()))
        values

    let res = f4 myHandler "number %d string %s float %f"  3 "test" 4.3
    let res2 format = f4 myHandler format
    let res3 = res2 "hello %s number %d" "name" 3
    printfn "res3 = %A" res3

    // We can also simply return the values
    let f5 (format:PrintfFormat<'PrintfType,_,_,'ReturnType>):'PrintfType=
        let rec consumer (myType:Type) (valuesList:obj list) (v:obj) =
            let domain,range = FSharpType.GetFunctionElements myType
            // accumulate types and lists
            let nextValuesList = (v::valuesList)
            if FSharpType.IsFunction range then
                let internalConsumer = consumer range nextValuesList
                box (FSharpValue.MakeFunction(range, internalConsumer))
            else
            // pass full lists to our handler
            // note that values have the right type in the handler
                box (List.rev nextValuesList)
        let argsConsumer = consumer typeof<'PrintfType> []
        unbox (FSharpValue.MakeFunction(typeof<'PrintfType>, argsConsumer))
        
    printfn "calling f5"
    let values5 = f5 "number %d string %s float %f"  3 "test" 4.3
    values5 |> List.iter (fun e -> printf "- %A(%A)" e (e.GetType()))
    printfn ""


    // storing the format string in a variable is possible if we set the type
    // explicitely
    let format:PrintfFormat<_,_,_,_> = "hello %s"
    printfn format "john"

    // if you get an `error FS0030: Value restriction` the solution is usually to
    // explicitly define the parameters expected by the format string
    // note that you make a function of the value, and the call has now to be
    // placed into parenthesis
    // For details, see:
    // http://mlton.org/ValueRestriction
    //    (in short: the rhs must be of value type to avoid inconsistencies with ref cells)
    // https://users.cs.fiu.edu/~smithg/cop4555/valrestr.html
    //    excerpt:
    //      in val x = e
    //      we can give x a polymorphic type only if e is a syntactic value (also known as a nonexpansive expression).
    //      Basically, a syntactic value is an expression that can be evaluated without doing any computation.
    // https://fsharp.org/specs/language-spec/4.1/FSharpSpec-4.1-latest.pdf
    //   The FSharp standard definition also states (12.1):
    //     Report a “value restriction” error if the resulting signature of any item that is not a member,
    //     constructor, function, or type function contains any free inference type variables.
    //   So the solution is to make our value a function that will return the PrintfFormat value.
    //   We can bind the result of the call and use that
    //   I think the two steps are needed as you cannot specify the return type of an anonymous function.
    let formatFun ():PrintfFormat<_,_,_,_> = "hello %s"
    let format2 = formatFun()
    printfn  format2  "johnny"

    // we can also write a conversion function, setting its argument type to PrintfFormat, triggering the
    // compiler to make the conversion
    // Remember the goal was to bind the PrintfFormat value to be able to reuse multiple timesi, which is
    // reached here
    let getPrintfFormat (s:PrintfFormat<_,_,_,_>) =
        s
    let format3 = getPrintfFormat "hello dear %s"
    printfn format3 "greg"
    printfn format3 "dan"



    0 // return an integer exit code
