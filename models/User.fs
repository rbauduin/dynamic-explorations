module User 
    // For SHA1 computation
    open System.Security.Cryptography
    // get DateTime
    open System
    open Error


    type User = {id             :int;  
                 account_id     :int;    
                 user_type_id   :int;     
                 login          :string;  
                 password       :string;  
                 email          :string;  
                 firstname      :string option;  
                 lastname       :string option;  
                 uuid           :string option;  
                 salt           :string;  
                 verified       :int;     
                 created_at     :DateTime;
                 updated_at     :DateTime option;
                 logged_in_at   :DateTime option;
                 api_key        :string option
                 }  

