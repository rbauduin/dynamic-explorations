module Email
    open System.Text.RegularExpressions
    open Error

    type Email = Email of string
    let fromString (s:string) : Result<Email,Errors> = 
        let pattern="[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,}"
        let m = Regex(pattern).Match(s)
        match m.Success with
        | true  -> Ok (Email s)
        | false -> Error ["Invalid email"]

