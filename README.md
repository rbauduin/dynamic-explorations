# Dynamic types in F#
As I wanted to reuse fixtures I had from a rails project, I had to work with dynamic typing in F#.
I wanted to to load records in a YAML files to a database, without having code specific for each table
loaded.
The file Program.fs is where to look. The beginning of the `main` function illustrates a bad approach, but
let me discover what's possible. The latter part of the file show a much better approach, as it is shorter, type safer.

After cloning this repo, you should be able to run the code with `dotnet run`  (after a `dotnet restore` if needed).

Many thanks to @v1rusw0rm and @mmiller on the F# Slack channel for their precious help!
